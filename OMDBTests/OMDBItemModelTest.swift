//
//  OMDBItemModelTest.swift
//  OMDBTests
//
//  Created by Appinventiv on 23/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import XCTest

@testable import OMDB
@testable import Pods_OMDB

class OMDBItemModelTest: XCTestCase {

    var itemModel: OMDBItemModel!

    
    override func setUp() {
        super.setUp()
        
        itemModel = OMDBItemModel(dict: [
            "Title" : "Batman: The Dark Knight Returns, Part 1",
            "Poster" : "https://m.media-amazon.com/images/M/MV5BMzIxMDkxNDM2M15BMl5BanBnXkFtZTcwMDA5ODY1OQ@@._V1_SX300.jpg",
            "Year" : "2012",
            "Type" : "movie",
            "imdbID" : "tt2313197"
        ])
    }

    override func tearDown() {
        super.tearDown()
        itemModel = nil
    }

    func testExample() {
        //Test it
        XCTAssertTrue(itemModel!.timeDiffrence > 1)
    }

    

}
