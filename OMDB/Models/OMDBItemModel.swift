//
//  OMDBItemModel.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import SwiftyJSON

struct OMDBItemModel {
    
    var imdbID: String
    var type: String
    var title: String
    var year: String
    var poster: String
    
    var timeDiffrence: Int {
        get {
            let currentYear = Date().year
            var releaseYear = Int(year)
            if year.contains(s: "-"), let lastYear = year.components(separatedBy: ",").last {
                releaseYear = Int(lastYear)
            }
            return (currentYear - (releaseYear ?? currentYear))
        }
    }
    var releaseYear: String {
        get {
            let diffrence = self.timeDiffrence
            if diffrence > 1 {
                return "\(diffrence) years ago"
            } else if diffrence == 1 {
                return "\(diffrence) year ago"
            } else {
                return "few months ago"
            }
        }
    }
    
    init(json: JSON) {
        imdbID = json[ApiKey.imdbID].stringValue
        type = json[ApiKey.type].stringValue
        title = json[ApiKey.Title].stringValue
        year = json[ApiKey.Year].stringValue
        poster = json[ApiKey.Poster].stringValue
    }
    init(dict: [String: Any]) {
        imdbID = dict[ApiKey.imdbID] as? String ?? ""
        type = dict[ApiKey.type] as? String ?? ""
        title = dict[ApiKey.Title] as? String ?? ""
        year = dict[ApiKey.Year] as? String ?? ""
        poster = dict[ApiKey.Poster] as? String ?? ""
    }
    
}

/*
 "imdbID" : "tt2975590",
 "Type" : "movie",
 "Title" : "Batman v Superman: Dawn of Justice",
 "Year" : "2016",
 "Poster" : "https:\/\/m.media-amazon.com\/images\/M\/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
 */
