//
//  OMDBItemsListVC.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class OMDBItemsListVC: BaseVC {

    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var omdbItemListCV: UICollectionView!
    
    //MARK:- Stored Property
    //======================
    var nextCount = 1
    let refreshController = UIRefreshControl()
    var itemsModels = [OMDBItemModel]()
    var isRequestingData = false

    //MARK:- View Life Cycle
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBar(withTitle: "OMDB")
    }
    
    
    //MARK:- @IBActions
    //=================
    @objc func pullToReloadData() {
        
        self.nextCount = 0
        self.isRequestingData = false
        self.hitGetMatchList()
    }
}

//MARK:- extension for private methods
//====================================
extension OMDBItemsListVC {
    
        private func initialSetup() {
            registerXibs()
            setUpForRefreshController()
            self.omdbItemListCV.delegate = self
            self.omdbItemListCV.dataSource = self
            self.omdbItemListCV.prefetchDataSource = self
            
    }
    
    private func registerXibs() {
        omdbItemListCV.registerCell(with: OMDBItemCell.self)
        omdbItemListCV.registerCell(with: LoaderCell.self)
    }
    
    fileprivate func setUpForRefreshController() {
        
        refreshController.addTarget(self,
                                    action: #selector(pullToReloadData),
                                    for: UIControl.Event.valueChanged)
        omdbItemListCV.addSubview(refreshController)
    }
}

//MARK: Extension for API calls
//=============================
extension OMDBItemsListVC {
    
    // MARK: Get Items List
    //=====================
        func hitGetMatchList() {
            guard  !isRequestingData && nextCount >= 0 else {
                return
            }
            isRequestingData = true
            let params = [ApiKey.s : "Batman",
                          ApiKey.page : nextCount,
                          ApiKey.apikey : "eeefc96f"] as [String : Any]
            
            WebServices.fetchItemList(parameters: params, success: { [weak self] (itemsList, totalCount) in
                
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.itemsModels.append(contentsOf: itemsList)
                strongSelf.nextCount = strongSelf.itemsModels.count < totalCount ? strongSelf.nextCount + 1 : 0
                strongSelf.omdbItemListCV.reloadData()
                strongSelf.refreshController.endRefreshing()
                strongSelf.isRequestingData = false
                }, failure: { [weak self] (err) in
                    guard let strongSelf = self else {
                        return
                    }
                    strongSelf.nextCount = 0
                    strongSelf.isRequestingData = false
                    strongSelf.refreshController.endRefreshing()
                    strongSelf.omdbItemListCV.reloadData()
            })
    }
}
