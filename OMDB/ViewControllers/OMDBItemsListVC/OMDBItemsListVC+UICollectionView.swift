//
//  OMDBItemsListVC+UICollectionView.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit
import Kingfisher
import Hero

//MARK:- extension for UICollectionView Delegate & DataSource
//===========================================================
extension OMDBItemsListVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.nextCount <= 0) ? itemsModels.count : (itemsModels.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == itemsModels.count {
            return collectionView.dequeueCell(with: LoaderCell.self, indexPath: indexPath)
        }
        
        let cell = collectionView.dequeueCell(with: OMDBItemCell.self, indexPath: indexPath)
        cell.populateData(model: self.itemsModels[indexPath.item])
        return cell
    }
    
}
//MARK:- extension for UICollectionView Delegate & DataSource
//===========================================================
extension OMDBItemsListVC:  UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item == itemsModels.count {
            return  CGSize(width: collectionView.width, height: 40)
        }
        if UIDevice.current.orientation.isLandscape {
           return CGSize(width: ((collectionView.width)/3)-10, height: (collectionView.height/2)*1.2)
        } else {
            return CGSize(width: collectionView.width/2, height: (collectionView.width/2)*1.2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
//MARK:- extension for UICollectionView Delegate & DataSource
//===========================================================
extension OMDBItemsListVC:  UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if cell is LoaderCell {
            self.hitGetMatchList()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item != itemsModels.count else {return}
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? OMDBItemCell else {return}
        let heroId = "cell\(indexPath.item)"
        cell.hero.id = heroId
        
        let omdbItemDetailScene = OMDBItemDetailVC.instantiate(fromAppStoryboard: .Main)
        omdbItemDetailScene.itemModel = self.itemsModels[indexPath.item]
        omdbItemDetailScene.hero.isEnabled = true
        omdbItemDetailScene.modalPresentationStyle = .overCurrentContext
        omdbItemDetailScene.headerView.hero.id = heroId
        let navigation = UINavigationController(rootViewController: omdbItemDetailScene)
        navigation.hero.isEnabled = true
        present(navigation, animated: true, completion: nil)
    }
}


//MARK:- extension for UICollectionView DataSource Prefetching
//===========================================================
extension OMDBItemsListVC: UICollectionViewDataSourcePrefetching {
    
    public func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (index) in
            if self.itemsModels.indices.contains(index.item), let url = URL(string: self.itemsModels[index.item].poster) {
                KingfisherManager.shared.downloader.downloadImage(with: url)
            }
        }
    }
}
