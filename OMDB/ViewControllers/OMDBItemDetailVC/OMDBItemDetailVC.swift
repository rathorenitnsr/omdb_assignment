//
//  OMDBItemDetailVC.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit
import MXParallaxHeader

class OMDBItemDetailVC: BaseVC {

    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var omdbItemDetailTV: UITableView!
    
    //MARK:- Stored Property
    //======================
    let headerView = OMDBDetailHeaderView.instanciateFromNib()
    var itemModel : OMDBItemModel?
    
    //MARK:- View Life Cycle
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBar(withTitle: "Detail", leftNavType: .cross)
    }
    
    override func onClickCrossNavigationBarButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- extension for private methods
//====================================
extension OMDBItemDetailVC {
    
    private func initialSetup() {
        registerXibs()
        
        self.omdbItemDetailTV.parallaxHeader.view = headerView
        headerView.frame = CGRect(x: 0, y: 0, width: self.omdbItemDetailTV.width, height: 250)
        self.omdbItemDetailTV.parallaxHeader.height = 250
        self.omdbItemDetailTV.parallaxHeader.mode = MXParallaxHeaderMode.fill
        self.omdbItemDetailTV.parallaxHeader.minimumHeight = 0
        
        
        headerView.posterImageView.setImage_kf(imageString: itemModel?.poster ?? "", placeHolderImage: #imageLiteral(resourceName: "placeholder"))
        
        self.omdbItemDetailTV.delegate = self
        self.omdbItemDetailTV.dataSource = self
    }
    
    private func registerXibs() {
        omdbItemDetailTV.registerCell(with: OMDBDetailCell.self)
        
    }
}
// MARK:- Extension for tableview Delegate And DataSource
//=======================================================
extension OMDBItemDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.itemModel != nil ? 3 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: OMDBDetailCell.self, indexPath: indexPath)
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "Name"
            cell.descriptionLabel.text = self.itemModel?.title ?? "N/A"
        case 1:
            cell.titleLabel.text = "Release Year"
            cell.descriptionLabel.text = self.itemModel?.releaseYear ?? "N/A"
        case 2:
            cell.titleLabel.text = "Category"
            cell.descriptionLabel.text = self.itemModel?.type ?? "N/A"
        default:
            fatalError("invalid index path")
        }
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemModel != nil ? UITableView.automaticDimension : CGFloat.leastNormalMagnitude
    }
    
}
