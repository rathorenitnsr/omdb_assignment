//
//  BaseVC.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    enum LeftNavType {
        case back,cross,none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
       

}

//MARK:- Extension for Navigation Methods
//=======================================
extension BaseVC {
    
    //MARK:- SET NAVIGATION BAR Methods
    //=================================
    func setNavigationBar(withTitle title:String? = nil,
                          leftNavType: LeftNavType? = nil,
                          firstRightBtnImg:UIImage? = nil,
                          
                          secRightBtnImg:UIImage? = nil,
                          thirdRightBtnImg:UIImage? = nil,
                          tintColor:UIColor? = nil,
                          barTintColor:UIColor? = nil,
                          attributesColor:UIColor? = nil) {
        
        //FOR LEFT BAR BUTTON
        var leftBtn = UIBarButtonItem()
        var firstRightBtn = UIBarButtonItem()
        
        
        if let lftNavType = leftNavType {
            if lftNavType == .back {
                leftBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickBackNavigationBarButton(_:)))
                
            } else if lftNavType == .cross  {
                leftBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "icCrossWhite"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickCrossNavigationBarButton(_:)))
                
            }
        }
        
        //FOR FIRST RIGHT BAR BUTTON
        if let rightBtnImg = firstRightBtnImg {
            let btn: UIButton = UIButton()
            btn.setImage(rightBtnImg, for: UIControl.State.normal)
            btn.addTarget(self, action: #selector(onClickFirstRightNavigationBarButton(_:)), for: UIControl.Event.touchUpInside)
            btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            firstRightBtn = UIBarButtonItem(customView: btn)
        }
        
        
        
        let tColor = (tintColor == nil) ?  UIColor.white : tintColor
        let barTColor = (barTintColor == nil) ?  UIColor.themeColor  : barTintColor
        
        let tAttributes = (attributesColor == nil) ? [
            NSAttributedString.Key.foregroundColor: tColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold)]:
            [
                NSAttributedString.Key.foregroundColor: attributesColor ?? tColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold)]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //    UIApplication.shared.statusBarStyle = .default
        
        
        self.updateNavigationBar(withTitle: title,
                                 leftButton: leftBtn,
                                 rightButton: [firstRightBtn],
                                 tintColor: tColor,
                                 barTintColor: barTColor,
                                 titleTextAttributes: tAttributes as [NSAttributedString.Key : Any])
    }
    
    @objc func onClickBackNavigationBarButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickCrossNavigationBarButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onClickFirstRightNavigationBarButton(_ sender: UIButton) {
        
    }
    
    @objc func onClickSecRightNavigationBarButton(_ sender: UIButton) {
        
    }
    
    @objc func onClickThirdRightNavigationBarButton(_ sender: UIButton) {
        
    }
}
