//
//  AppNetworking.swift
//  NewProject
//
//  Created by  on 30/08/18.
//  Copyright © 2018 . All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import Photos

typealias JSONDictionary = [String : Any]
typealias JSONDictionaryArray = [JSONDictionary]
typealias SuccessResponse = (_ json : JSON) -> ()
typealias FailureResponse = (NSError) -> (Void)
typealias ResponseMessage = (_ message : String) -> ()
typealias OMDBItemListResponse = (_ data : [OMDBItemModel], _ totalCount: Int) -> ()



extension Notification.Name {
    static let NotConnectedToInternet = Notification.Name("NotConnectedToInternet")
}

enum AppNetworking {
    
    static let username = "admin"
    static let password = "12345"

    static func POST(endPoint : String,
                     parameters : JSONDictionary = [:],
                     headers : HTTPHeaders = [:],
                     loader : Bool = true,
                     success : @escaping (JSON) -> Void,
                     failure : @escaping (NSError) -> Void) {
        
        
        request(URLString: endPoint, httpMethod: .post, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    
    
    static func GET(endPoint : String,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    static func PUT(endPoint : String,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .put, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    static func PATCH(endPoint : String,
                      parameters : JSONDictionary = [:],
                      encoding: URLEncoding = URLEncoding.httpBody,
                      headers : HTTPHeaders = [:],
                      loader : Bool = true,
                      success : @escaping SuccessResponse,
                      failure : @escaping FailureResponse) {
        
        request(URLString: endPoint, httpMethod: .patch, parameters: parameters, encoding: encoding, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    static func DELETE(endPoint : String,
                       parameters : JSONDictionary = [:],
                       headers : HTTPHeaders = [:],
                       loader : Bool = true,
                       success : @escaping (JSON) -> Void,
                       failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .delete, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    
    
    private static func request(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: ParameterEncoding = JSONEncoding.default,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                failure : @escaping (NSError) -> Void) {
        
        if loader { CommonFunctions.showActivityLoader() }
        
        makeRequest(URLString: URLString, httpMethod: httpMethod, parameters: parameters, encoding: encoding, headers: headers, loader: loader, success: { (json) in
            if loader { CommonFunctions.hideActivityLoader() }
            success(json)
        }, failure: failure)
    }
    
    private static func makeRequest(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: ParameterEncoding = JSONEncoding.default,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                failure : @escaping (NSError) -> Void) {
        let updatedHeaders : HTTPHeaders = headers
        
        

        Alamofire.request(URLString, method: httpMethod, parameters: parameters, encoding: encoding, headers: updatedHeaders).responseJSON { (response:DataResponse<Any>) in
            
            if loader { CommonFunctions.hideActivityLoader() }
            
            printDebug("==== METHOD ====")
            printDebug(httpMethod)
            printDebug("==== ENCODING ====")
            printDebug(encoding)
            printDebug("==== URL STRING ====")
            printDebug(URLString)
            printDebug("===== HEADERS ====")
            printDebug(headers)
            printDebug("==== PARAMETERS ====")
            printDebug(parameters.description)
            
            switch(response.result) {
            case .success(let value):
                printDebug("==== RESPONSE ====")
                printDebug(JSON(value))
                
                let json = JSON(value)
                success(json)
                
            case .failure(let e):
                printDebug("===== FAILURE ====")
                printDebug(e.localizedDescription)
                if loader { CommonFunctions.hideActivityLoader() }
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                    
                    CommonFunctions.showMessage(LocalizedStrings.pleaseCheckInternetConnection.localized)
                    failure(e as NSError)
                    
                } else {
                    failure(e as NSError)
                }
            }
        }
    }
    
    
    
}
