//
//  Webservices.swift
//  NewProject
//
//  Created by  on 30/08/18.
//  Copyright © 2018 . All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

extension WebServices {
    
    // MARK:- Common POST API
    //=======================
    static func commonPostAPI(parameters: JSONDictionary,
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              failure : @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
            success(json)
            /*
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            switch code {
            case ApiCode.success, 203: success(json)
            default: failure(NSError(code: code, localizedDescription: msg))
            }
             */
        }) { (error) in
            failure(error)
        }
    }
    
    // MARK:- Common Get API
    //=======================
    static func commonGetAPI(parameters: JSONDictionary,
                             endPoint: EndPoint,
                             loader: Bool = true,
                             success : @escaping SuccessResponse,
                             failure : @escaping FailureResponse) {
        
        AppNetworking.GET(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
            success(json)
            /*
            let code = json[ApiKey.code].intValue
            let msg = json[ApiKey.message].stringValue
            switch code {
            case ApiCode.success, 203: success(json)
            default: failure(NSError(code: code, localizedDescription: msg))
            }
             */
        }) { (error) in
            failure(error)
        }
    }
    
    // MARK:- Items List
    //==================
    static func fetchItemList(parameters: JSONDictionary,
                              loader: Bool = true,
                              success: @escaping OMDBItemListResponse,
                              failure: @escaping FailureResponse) {
        
        
        self.commonGetAPI(parameters: parameters, endPoint: .itemsList, success: { (json) in
            
            var model = [OMDBItemModel]()
            for object in json[ApiKey.Search].arrayValue {
                model.append(OMDBItemModel(json: object))
            }
            success(model, json[ApiKey.totalResults].intValue)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    
    
    
    
    
    
    
}

