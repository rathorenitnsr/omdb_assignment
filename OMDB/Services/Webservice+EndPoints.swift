//
//  Webservice+EndPoints.swift
//  NewProject
//
//  Created by  on 30/08/18.
//  Copyright © 2018 . All rights reserved.
//

import Foundation

let baseUrl = "http://www.omdbapi.com/"

//http://www.omdbapi.com/?s=Batman&page=1&apikey=eeefc96f
extension WebServices {
    
    enum EndPoint : String {
        
        case itemsList = ""
        
        var path : String {
            
            let url = baseUrl
            return url + self.rawValue
        }
    }
}
