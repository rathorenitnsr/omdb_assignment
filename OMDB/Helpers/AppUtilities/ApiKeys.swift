//
//  UserInputKeys.swift
//  
//
//  Created by OMDB on 05/02/18.
//  Copyright © 2018 . All rights reserved.
//

import Foundation

//MARK:- Api Keys
//=======================
enum ApiKey {
    
    static var s: String { return "s" }
    static var page: String { return "page" }
    static var apikey: String { return "apikey" }
    static var imdbID: String { return "imdbID" }
    static var type: String { return "Type" }
    static var Title: String { return "Title" }
    static var Year: String { return "Year" }
    static var Poster: String { return "Poster" }
    static var Search: String { return "Search" }
    static var totalResults: String { return "totalResults" }

    
}

//MARK:- Api Code
//=======================
enum ApiCode {
    
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
}
