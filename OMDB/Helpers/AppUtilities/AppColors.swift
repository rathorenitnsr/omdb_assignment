//
//  AppColors.swift
//  
//
//  Created by on 20/04/17.
//  Copyright © 2017  All rights reserved.
//

import UIKit

extension UIColor {
    
    static var themeColor: UIColor                  { return #colorLiteral(red: 0.1529411765, green: 0.07058823529, blue: 1, alpha: 1) } // rgb 39 18 255

}
