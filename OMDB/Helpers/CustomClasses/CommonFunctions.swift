//
//  CommonClasses.swift
//  
//
//  Created by  on 23/11/17.
//  Copyright © 2017 . All rights reserved.
//

import MobileCoreServices
import SwiftEntryKit

class CommonFunctions {

    private static var attributes: EKAttributes = {
        var attributes = EKAttributes()
        attributes.positionConstraints = .fullWidth
        attributes.hapticFeedbackType = .success
        attributes.positionConstraints.safeArea = .empty(fillSafeArea: true)
        attributes.entryBackground = .visualEffect(style: .light)
        return attributes
    }()
    
    /// Show Toast With Message
    static func showMessage(_ msg: String, completion: (() -> Swift.Void)? = nil) {

        DispatchQueue.mainQueueAsync {
            
            let title = EKProperty.LabelContent(text: "Error!", style: EKProperty.LabelStyle(font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold), color: .black))
            let description = EKProperty.LabelContent(text: msg, style: EKProperty.LabelStyle(font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light), color: .black))
            let simpleMessage = EKSimpleMessage(image: nil, title: title, description: description)
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            
            let contentView = EKNotificationMessageView(with: notificationMessage)
            
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
    }
    
    /// Delay Functions
    class func delay(delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure()
            
        }
    }
    
    /// Show Action Sheet With Actions Array
    class func showActionSheetWithActionArray(_ title: String?, message: String?,
                                              viewController: UIViewController,
                                              alertActionArray : [UIAlertAction],
                                              preferredStyle: UIAlertController.Style)  {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        alertActionArray.forEach{ alert.addAction($0) }
        
        DispatchQueue.mainQueueAsync {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

    /// Show Activity Loader
    class func showActivityLoader() {
        
    }
    
    /// Hide Activity Loader
    class func hideActivityLoader() {
        
    }
}
