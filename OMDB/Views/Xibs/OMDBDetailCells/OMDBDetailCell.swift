//
//  OMDBDetailCell.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class OMDBDetailCell: UITableViewCell {
    
    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    
    //MARK:- Cell Life Cycle
    //=======================
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetup()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
//MARK:- extension for private methods
//====================================
extension OMDBDetailCell {
    
    //MARK:- initialization private method
    //====================================
    private func initialSetup() {
        
        self.titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        self.descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        
        self.titleLabel.textColor = UIColor.black
        self.descriptionLabel.textColor = UIColor.black
    }
}
