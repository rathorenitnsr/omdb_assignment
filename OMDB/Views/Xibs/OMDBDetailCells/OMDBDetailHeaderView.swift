//
//  OMDBDetailHeaderView.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class OMDBDetailHeaderView: UIView {

    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var posterImageView: UIImageView!    
    
    
    //MARK:- Cell Life Cycle
    //======================
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetup()
    }
    
    //MARK:- Class Function
    //======================
    class func instanciateFromNib() -> OMDBDetailHeaderView {
        return Bundle .main .loadNibNamed("OMDBDetailHeaderView", owner: self, options: nil)![0] as! OMDBDetailHeaderView
    }
    
}

//MARK:- extension for private methods
//====================================
extension OMDBDetailHeaderView {
    
    private func initialSetup() {
        self.backgroundColor = UIColor.white
    }
}
