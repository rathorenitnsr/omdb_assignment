//
//  LoaderCell.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class LoaderCell: UICollectionViewCell {
    
    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:- Cell Life Cycle
    //======================
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.activityIndicator.startAnimating()
    }
}
//MARK:- extension for private methods
//====================================
extension LoaderCell {
    
    private func initialSetup() {
        
        self.activityIndicator.startAnimating()
    }
}
