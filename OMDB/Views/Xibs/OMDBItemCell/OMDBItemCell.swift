//
//  OMDBItemCell.swift
//  OMDB
//
//  Created by  on 22/12/18.
//  Copyright © 2018 MARSPLAY. All rights reserved.
//

import UIKit

class OMDBItemCell: UICollectionViewCell {
   
    //MARK:- @IBOutlets
    //=================
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    //MARK:- Cell Life Cycle
    //======================
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    
}
//MARK:- extension for private methods
//====================================
extension OMDBItemCell {
    
    private func initialSetup() {
        
        for label in [titleLabel,yearLabel,typeLabel] {
            label?.textColor = UIColor.black
            label?.font = UIFont.systemFont(ofSize: 12)
        }
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        posterImageView.round(radius: 5)
    }
}

//MARK:- extension for Public methods
//====================================
extension OMDBItemCell {
    
    public func populateData(model: OMDBItemModel) {
        titleLabel.text = model.title
        yearLabel.text = "Release: \(model.releaseYear)"
        typeLabel.text = "Category: \(model.type)"
        posterImageView.setImage_kf(imageString: model.poster, placeHolderImage: #imageLiteral(resourceName: "placeholder"))
    }
}
